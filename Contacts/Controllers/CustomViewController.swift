//
//  CustomViewController.swift
//  Contacts
//
//  Created by Vitalii Kravets on 6/4/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

class CustomViewController<V: UIView>: UIViewController {
	var customView: V {
		guard let customView = view as? V else {
			fatalError("Expected view to be of type \(V.self) but got \(type(of: view)) instead")
		}

		return customView
	}

	override func loadView() {
		let customView = V()
		view = customView
		view.backgroundColor = .systemBackground
	}
}
