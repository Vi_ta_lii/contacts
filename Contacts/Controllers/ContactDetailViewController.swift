//
//  ContactDetailViewController.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/20/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

private struct Constants {
	static let buttonTitle = "Back"
}

final class ContactDetailViewController: CustomViewController<ContactDetailView> {
	// MARK: - Subviews

	private let backButton: UIButton = {
		let button = UIButton(type: .system)
		button.setTitle(Constants.buttonTitle, for: .normal)
		button.translatesAutoresizingMaskIntoConstraints = false
		button.addTarget(self, action: #selector(dismissSelf), for: .touchUpInside)
		return button
	}()

	// MARK: - Properties

	var contact: Contact? {
		didSet {
			guard contact != oldValue, let contact = contact else { return }
			customView.contactViewModel = ContactViewModel(contact: contact)
		}
	}

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()
		view.addSubview(backButton)
		constraintSubviews()
	}

	// MARK: - Private methods

	private func constraintSubviews() {
		NSLayoutConstraint.activate([
			backButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
			backButton.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor)
		])
	}

	@objc private func dismissSelf(_ sender: UIButton) {
		dismiss(animated: true)
	}
}
