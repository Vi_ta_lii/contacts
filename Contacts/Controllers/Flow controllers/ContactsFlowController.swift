//
//  ContactsFlowController.swift
//  Contacts
//
//  Created by Vitalii Kravets on 6/4/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

final class ContactsFlowController: UIViewController {
	// MARK: - Properties

	private lazy var contactsVC: ContactsViewController = {
		let controller = ContactsViewController()
		controller.didSelectContact = { [weak self] contact in
			self?.showContactDetailViewController(with: contact)
			HeroID.id += 1
		}

		return controller
	}()

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()
		install(contactsVC)
	}

	// MARK: - Private methods

	private func showContactDetailViewController(with contact: Contact) {
		let controller = ContactDetailViewController()
		controller.contact = contact
		controller.modalPresentationStyle = .fullScreen
		controller.hero.isEnabled = true
		controller.customView.avatarImageView.hero.id = "avatar\(HeroID.id)"
		show(controller, sender: self)
	}
}
