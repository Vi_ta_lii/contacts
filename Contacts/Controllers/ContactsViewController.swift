//
//  ContactsViewController.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit
import Hero

final class ContactsViewController: CustomViewController<ContactsView> {
	// MARK: - Properties

	var didSelectContact: ((Contact) -> Void)?
	private var contactsDataSource = ContactsDataSource()

	// MARK: - Lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .systemBackground
		customView.delegate = self
		contactsDataSource.configure(for: customView.collectionView)
	}
}

// MARK: - ContactsViewDelegate

extension ContactsViewController: ContactsViewDelegate {

	func contactView(_ contactView: ContactsView, didSelectItemAt indexPath: IndexPath) {
		guard let contact = contactsDataSource.itemIdentifier(for: indexPath),
			let cell = contactView.collectionView.cellForItem(at: indexPath) as? ContactCell else { return }

		cell.avatarImageView.hero.id = "avatar\(HeroID.id)"
		didSelectContact?(contact)
	}

	func contactView(_ contactView: ContactsView, didTapSimulateChangesButton button: UIButton) {
		contactsDataSource.simulateChanges()
	}
}
