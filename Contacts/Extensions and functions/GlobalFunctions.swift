//
//  GlobalFunctions.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/13/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

func toString(_ cls: AnyClass) -> String {
    return String(describing: cls)
}
