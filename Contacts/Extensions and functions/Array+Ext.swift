//
//  Array+Ext.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/21/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

extension Array {

	func randomUniqueElements(_ numberOfElements: Int) -> Array {
		if numberOfElements >= self.count {
			return self
		} else if numberOfElements < 0 {
			return []
		}

		var indecies = [Int]()
		indecies.reserveCapacity(numberOfElements)
		(1...numberOfElements).forEach { _ in
			var randomElement = Int.random(in: 0..<self.count)
			while indecies.contains(randomElement) {
				randomElement = Int.random(in: 0..<self.count)
			}

			indecies.append(randomElement)
		}

		let randomElements = indecies.map { self[$0] }

		return randomElements
	}
}
