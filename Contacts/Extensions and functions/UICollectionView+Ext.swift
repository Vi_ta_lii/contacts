//
//  UICollectionView+Ext.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/13/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

extension UICollectionView {
	func register(_ cellClass: AnyClass) {
		register(cellClass, forCellWithReuseIdentifier: toString(cellClass))
	}

	func dequeueReusableCell(withCellClass cellClass: AnyClass, for indexPath: IndexPath) -> UICollectionViewCell {
		return self.dequeueReusableCell(withReuseIdentifier: toString(cellClass), for: indexPath)
    }
}
