//
//  Publisher+Ext.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/23/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import Combine

extension Publisher where Failure == Never {
    public func assignNoRetain<Root>(to keyPath: ReferenceWritableKeyPath<Root, Output>,
									 on object: Root) -> AnyCancellable where Root: AnyObject {
        sink { [weak object] value in
        object?[keyPath: keyPath] = value
    }
  }
}
