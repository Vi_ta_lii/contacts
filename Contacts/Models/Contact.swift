//
//  Contact.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/14/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

final class Contact {

	// MARK: - Properties

	let id = UUID()
	var name: String
	let email: String
	var onlineStatus: OnlineStatus

	// MARK: - Init

	init(name: String, email: String, onlineStatus: OnlineStatus) {
		self.name = name
		self.email = email
		self.onlineStatus = onlineStatus
	}
}

// MARK: - Hashable

extension Contact: Hashable {

	static func == (lhs: Contact, rhs: Contact) -> Bool {
		return lhs.id == rhs.id
			&& lhs.name == rhs.name
			&& lhs.email == rhs.email
			&& lhs.onlineStatus == rhs.onlineStatus
    }

    func hash(into hasher: inout Hasher) {
		hasher.combine(id)
        hasher.combine(name)
        hasher.combine(email)
		hasher.combine(onlineStatus)
    }
}
