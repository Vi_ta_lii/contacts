//
//  ImageLoader.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/18/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit
import Combine

private struct Constants {
	static let numberOfRetries = 2
}

final class ImageLoader {

	// MARK: - Properties

	private let session: URLSession

	// MARK: - Init

	init(session: URLSession) {
		self.session = session
	}

	// MARK: - Methods

	func load(from url: URL) -> AnyPublisher<UIImage?, Never> {
		session.dataTaskPublisher(for: url)
			.retry(Constants.numberOfRetries)
			.subscribe(on: DispatchQueue.global())
			.map { UIImage(data: $0.data) }
			.replaceError(with: #imageLiteral(resourceName: "icon"))
			.receive(on: DispatchQueue.main)
			.eraseToAnyPublisher()
	}
}
