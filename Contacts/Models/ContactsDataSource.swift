//
//  ContactsDataSource.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/13/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

private struct Constants {
	static let generatedContacts = 15
}

struct ContactsDataSource {

	private typealias DataSource = UICollectionViewDiffableDataSource<CollectionViewSection, Contact>
	private typealias Snapshot = NSDiffableDataSourceSnapshot<CollectionViewSection, Contact>

	// MARK: - Properties

	private var dataSource: DataSource?
	private let dataProvider = ContactsDataProvider()
	private let dataSourceQueue = DispatchQueue(label: "\(ContactsDataSource.self)Queue", qos: .background)

	// MARK: - Methods

	func itemIdentifier(for indexPath: IndexPath) -> Contact? {
		return dataSource?.itemIdentifier(for: indexPath)
	}

	mutating func configure(for collectionView: UICollectionView) {
		dataSource = DataSource(collectionView: collectionView, cellProvider: cellProvider)
		populateWithInitialData()
	}

	func simulateChanges() {
		simulateNamesChange()
		simulateOnlineStatusChange()
		simulateRemovals()
		simulateAdditions()
	}

	// MARK: - Private methods

	private let cellProvider: (UICollectionView, IndexPath, Contact) -> UICollectionViewCell? = {
		collectionView, indexPath, contact in
		guard let cell = collectionView.dequeueReusableCell(withCellClass: ContactCell.self,
															for: indexPath) as? ContactCell else {
			fatalError("Cannot create a new cell")
		}

		cell.contactViewModel = ContactViewModel(contact: contact)
		return cell
	}

	private func populateWithInitialData() {
		var snapshot = Snapshot()
		snapshot.appendSections([.main])
		let data = dataProvider.generateContacts(number: Constants.generatedContacts)
		snapshot.appendItems(data)
		applyInBackground(snapshot, to: dataSource, animatingDifferences: false)
	}

	private func applyInBackground(_ snapshot: Snapshot,
								   to dataSource: DataSource?,
								   animatingDifferences: Bool,
								   completion: (() -> Void)? = nil) {
		dataSourceQueue.async {
			dataSource?.apply(snapshot, animatingDifferences: animatingDifferences, completion: completion)
		}
	}

	private func simulateNamesChange() {
		guard var snapshot = dataSource?.snapshot() else { return }

		let contacts = snapshot.itemIdentifiers
		dataProvider.names.randomUniqueElements(contacts.count).enumerated().forEach {
			contacts[$0].name = $1
		}

		snapshot.reloadItems(contacts)
		applyInBackground(snapshot, to: dataSource, animatingDifferences: true)
	}

	private func simulateOnlineStatusChange() {
		guard var snapshot = dataSource?.snapshot() else { return }

		let contacts = snapshot.itemIdentifiers
		let onlineStatuses = OnlineStatus.allCases

		contacts.forEach { $0.onlineStatus = onlineStatuses.randomElement() ?? $0.onlineStatus }
		snapshot.reloadItems(contacts)

		applyInBackground(snapshot, to: dataSource, animatingDifferences: true)
	}

	private func simulateRemovals() {
		guard var snapshot = dataSource?.snapshot() else { return }

		let contacts = snapshot.itemIdentifiers
		let oneThird = contacts.count / 3
		guard oneThird >= 1 else { return }
		let numberOfContactsToRemove = Int.random(in: 1...oneThird)

		let contactsToRemove = contacts.randomUniqueElements(numberOfContactsToRemove)
		snapshot.deleteItems(contactsToRemove)

		applyInBackground(snapshot, to: dataSource, animatingDifferences: true)
	}

	private func simulateAdditions() {
		guard var snapshot = dataSource?.snapshot() else { return }

		let contacts = snapshot.itemIdentifiers
		guard let randomContact = contacts.randomElement() else { return }
		let numberOfContactsToAdd = Int.random(in: 1...5)

		let contactsToAdd = dataProvider.generateContacts(number: numberOfContactsToAdd)

		snapshot.insertItems(contactsToAdd, beforeItem: randomContact)
		applyInBackground(snapshot, to: dataSource, animatingDifferences: true)
	}
}
