//
//  ContactsDataProvider.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/14/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

struct ContactsDataProvider {

	private let emailDomains = ["@live.com",
								"@aol.com",
								"@msn.com",
								"@me.com",
								"@gmail.com"]

	let names = ["Azalee Gentle",
				 "Carlyn Lucius",
				 "Barbie Kindred",
				 "Lashawn Silvas",
				 "Song Poplar",
				 "Rosemary Mcnemar",
				 "Shawn Tatem",
				 "Petrina Janney",
				 "Eva Yedinak",
				 "Clemente Scroggins",
				 "Melanie Kubicek",
				 "Ronni Hickle",
				 "Sherie Fukushima",
				 "Sherril Dismuke",
				 "In Lebsock",
				 "Carmine Mastropietro",
				 "Yong Ismail",
				 "Maurita Sunderland",
				 "Mayra Longworth",
				 "Li Kilroy",
				 "Theron Budzinski",
				 "Angelo Langhorne",
				 "Tai Oppenheim",
				 "Woodrow Neri",
				 "Christiane Blumberg",
				 "Anika Wallach",
				 "Page Lamberti",
				 "Timika Delamora",
				 "Harlan Kral",
				 "Andria Western",
				 "Ashly Baro",
				 "Deane Evens",
				 "Jermaine Carballo",
				 "Lakisha Ort",
				 "Derrick Kindell",
				 "Shayla Legette",
				 "Dot Ploof",
				 "Doloris Blazek",
				 "Angelia Brazil",
				 "Jaqueline Mcmasters",
				 "Morris Daum",
				 "Janeth Darby",
				 "Breann Stinger",
				 "Ebony Venable",
				 "Russel Harkness",
				 "Ismael Giles",
				 "Mona Yousef",
				 "Sherman Grave",
				 "Kala Giesen",
				 "Russ Pylant",
				 "Charlena Ehrlich",
				 "Zaida Dardar",
				 "Ula Rattler",
				 "Lewis Diggs",
				 "Zoila Swinehart",
				 "Dalene Lainez",
				 "Rickie Hershman",
				 "Janine Nyman",
				 "Janell Keo",
				 "Nellie Mcnett",
				 "Maryanne Leiser",
				 "Joanne Riles",
				 "Weldon Thomure",
				 "Twila Hargett",
				 "Linn Langenfeld",
				 "Shon Ansari",
				 "Verdie Fansler",
				 "Tasia Epperly",
				 "Camille Spates",
				 "Grayce Fitchett",
				 "Suzanne Pica",
				 "Arnulfo Humphreys",
				 "Katelin Cole",
				 "Kanesha Madlock",
				 "Abel Hickson",
				 "Wanetta Fazekas",
				 "Shawnta Datta",
				 "Sonya Nicolas",
				 "Bennie Placek",
				 "Lashay Cosner",
				 "Adriana Larin",
				 "Harlan Senter",
				 "Jessika Schewe",
				 "Cheyenne Groman",
				 "Cami Shumaker",
				 "Rolland Wessner",
				 "Sheridan Chard",
				 "Ocie Stoughton",
				 "Ciara Uhrich",
				 "Sha Orourke",
				 "Juliane Bonnette",
				 "Elbert Haston",
				 "Sanda Leggio",
				 "Arcelia Kiger",
				 "Carolann Turmelle",
				 "Kaylene Pfarr",
				 "Lionel Towell",
				 "Genesis Selders",
				 "Veda Jason",
				 "Darcie Paulin"
	]

	/// Specify `number` of contacts in range 1...100
	func generateContacts(number: Int) -> [Contact] {
		guard number > 0 else { return [] }
		let namesCount = names.count
		let upperBound = number < namesCount ? number : namesCount

		return (1...upperBound).reduce([]) { (accum, _) in
			let name = names.randomElement() ?? "John Smith"
			let email = name.replacingOccurrences(of: " ", with: ".").lowercased() + (emailDomains.randomElement() ?? "@aol.com")
			let onlineStatus = OnlineStatus.allCases.randomElement() ?? .online
 			return accum + [Contact(name: name, email: email, onlineStatus: onlineStatus)]
		}
	}
}
