//
//  OnlineStatus.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/14/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

enum OnlineStatus: CaseIterable {
	// MARK: - Cases

	case online
	case away
	case doNotDisturb
	case offline

	// MARK: - Properties

	var icon: UIImage {
		switch self {
		case .online:
			return #imageLiteral(resourceName: "online_icon")
		case .away:
			return #imageLiteral(resourceName: "away_icon")
		case .doNotDisturb:
			return #imageLiteral(resourceName: "do_not_disturb_icon")
		case .offline:
			return #imageLiteral(resourceName: "offline_icon")
		}
	}

	var textualRepresentation: String {
		switch self {
		case .online:
			return "online"
		case .away:
			return "away"
		case .doNotDisturb:
			return "do not disturb"
		case .offline:
			return "offline"
		}
	}
}
