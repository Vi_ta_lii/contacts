//
//  ContactViewModel.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/20/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit
import Combine

final class ContactViewModel {

	// MARK: - Properties

	private let contact: Contact
	@Published private(set) var image: UIImage?
	private let imageLoader: ImageLoader
	private var cancellable: AnyCancellable?

	var onlineStatusImage: UIImage {
		return contact.onlineStatus.icon
	}

	var onlineStatusText: String {
		return contact.onlineStatus.textualRepresentation
	}

	var email: String {
		return contact.email
	}

	var name: String {
		return contact.name
	}

	// MARK: - Init

	init(contact: Contact) {
		self.contact = contact
		imageLoader = ImageLoader(session: URLSession(configuration: .default))
	}

	// MARK: - Methods

	func getAvatarImage(of size: Int) {
		let urlString = "https://www.gravatar.com/avatar/\(email.hash)?d=retro&s=\(size)"
		guard let url = URL(string: urlString) else { return }
		cancellable = imageLoader.load(from: url).assignNoRetain(to: \.image, on: self)
	}

	func cancelImageLoad() {
		cancellable?.cancel()
	}
}
