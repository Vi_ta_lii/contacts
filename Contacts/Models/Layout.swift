//
//  Layout.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

private struct Constants {
	static let itemWidth: CGFloat = 50
	static let groupHeight: CGFloat = 50
	static let inset: CGFloat = 10
}

enum Layout: Int, CaseIterable {
	// MARK: - Cases

	case list
	case grid

	// MARK: - Properties

	var currentLayout: UICollectionViewCompositionalLayout {
		let itemWidthDimension: NSCollectionLayoutDimension
		switch self {
		case .list:
			itemWidthDimension = .fractionalWidth(1.0)
		case .grid:
			itemWidthDimension = .absolute(Constants.itemWidth)
		}

		let itemSize = NSCollectionLayoutSize(widthDimension: itemWidthDimension,
											  heightDimension: .fractionalHeight(1.0))
		let item = NSCollectionLayoutItem(layoutSize: itemSize)

		let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
											   heightDimension: .absolute(Constants.groupHeight))
		let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
		group.interItemSpacing = .fixed(Constants.inset)
		group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: Constants.inset, bottom: 0, trailing: Constants.inset)

		let section = NSCollectionLayoutSection(group: group)
		section.interGroupSpacing = Constants.inset
		let layout = UICollectionViewCompositionalLayout(section: section)

		return layout
	}
}
