//
//  ContactDetailView.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/20/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit
import Combine

private struct Constants {
	static let avatarSize = 400
	static let avatarTopIndent: CGFloat = 80
	static let emailHeight: CGFloat = 30
	static let avatarLabelsIndent: CGFloat = 20
}

final class ContactDetailView: UIView {

	// MARK: - Subviews

	let avatarImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.contentMode = .scaleAspectFit
		imageView.translatesAutoresizingMaskIntoConstraints = false
		return imageView
	}()

	private lazy var labelsStackView: UIStackView = {
		let stackView = UIStackView(arrangedSubviews: [nameLabel, onlineStatusLabel, emailTextView])
		stackView.axis = .vertical
		stackView.alignment = .center
		stackView.translatesAutoresizingMaskIntoConstraints = false
		return stackView
	}()

	private let nameLabel: UILabel = {
		let label = UILabel()
		return label
	}()

	private let onlineStatusLabel: UILabel = {
		let label = UILabel()
		return label
	}()

	private let emailTextView: UITextView = {
		let textView = UITextView()
		textView.font = .systemFont(ofSize: 17)
		textView.textAlignment = .center
		textView.isEditable = false
		textView.isScrollEnabled = false
		textView.dataDetectorTypes = .all
		return textView
	}()

	// MARK: - Properties

	var contactViewModel: ContactViewModel? {
		didSet {
			contactViewModel?.getAvatarImage(of: Constants.avatarSize)
			cancellable = contactViewModel?.$image.assignNoRetain(to: \.avatarImageView.image, on: self)
			nameLabel.text = contactViewModel?.name
			onlineStatusLabel.text = contactViewModel?.onlineStatusText
			emailTextView.text = contactViewModel?.email
		}
	}

	private var cancellable: AnyCancellable?

	// MARK: - Init

	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private methods

	private func commonInit() {
		addSubviews()
		addConstraints()
	}

	private func addSubviews() {
		[avatarImageView, labelsStackView].forEach { addSubview($0) }
	}

	private func addConstraints() {
		NSLayoutConstraint.activate([
			avatarImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
			avatarImageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: Constants.avatarTopIndent),
			avatarImageView.widthAnchor.constraint(equalTo: avatarImageView.heightAnchor),

			emailTextView.heightAnchor.constraint(equalToConstant: Constants.emailHeight),
			emailTextView.widthAnchor.constraint(equalTo: widthAnchor),
			labelsStackView.topAnchor.constraint(equalTo: centerYAnchor),
			labelsStackView.topAnchor.constraint(equalTo: avatarImageView.bottomAnchor, constant: Constants.avatarLabelsIndent),
			labelsStackView.centerXAnchor.constraint(equalTo: centerXAnchor)
		])
	}
}
