//
//  ContactsView.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

private struct Constants {
	static let indent: CGFloat = 20
	static let switchWidth: CGFloat = 200
	static let buttonTitle = "Simulate Changes"
}

protocol ContactsViewDelegate: AnyObject {
	func contactView(_ contactView: ContactsView, didSelectItemAt indexPath: IndexPath)
	func contactView(_ contactView: ContactsView, didTapSimulateChangesButton button: UIButton)
}

final class ContactsView: UIView {

	// MARK: - Subviews

	private let layoutSwitch: UISegmentedControl = {
		let items = Layout.allCases.map { String("\($0)").capitalized }
		let layoutSwitch = UISegmentedControl(items: items)
		layoutSwitch.translatesAutoresizingMaskIntoConstraints = false
		layoutSwitch.selectedSegmentIndex = 0
		layoutSwitch.addTarget(self, action: #selector(changeLayout(_:)), for: .valueChanged)

		return layoutSwitch
	}()

	let collectionView: UICollectionView = {
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: Layout.list.currentLayout)
		collectionView.translatesAutoresizingMaskIntoConstraints = false
		collectionView.register(ContactCell.self)
		collectionView.backgroundColor = .systemBackground

		return collectionView
	}()

	private let simulateChangesButton: UIButton = {
		let button = UIButton(type: .system)
		button.translatesAutoresizingMaskIntoConstraints = false
		button.setTitle(Constants.buttonTitle, for: .normal)
		button.addTarget(self, action: #selector(simulateChanges(_:)), for: .touchUpInside)

		return button
	}()

	// MARK: - Properties

	weak var delegate: ContactsViewDelegate?

	private var layout: Layout = .list {
		didSet {
			guard layout != oldValue else { return }
			collectionView.showsVerticalScrollIndicator = false
			collectionView.setCollectionViewLayout(layout.currentLayout, animated: true)
			collectionView.showsVerticalScrollIndicator = true
			guard let ip = collectionView.indexPathsForVisibleItems.min() else { return }
			collectionView.scrollToItem(at: ip, at: .top, animated: true)
		}
	}

	// MARK: - Init

	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Private methods

	private func commonInit() {
		collectionView.delegate = self
		addSubviews()
		addConstraints()
	}

	private func addSubviews() {
		[layoutSwitch, collectionView, simulateChangesButton].forEach { addSubview($0) }
	}

	private func addConstraints() {
		NSLayoutConstraint.activate([
			layoutSwitch.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
			layoutSwitch.centerXAnchor.constraint(equalTo: centerXAnchor),
			layoutSwitch.widthAnchor.constraint(equalToConstant: Constants.switchWidth),

			collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
			collectionView.topAnchor.constraint(equalTo: layoutSwitch.bottomAnchor, constant: Constants.indent),
			collectionView.trailingAnchor.constraint(equalTo: trailingAnchor),

			simulateChangesButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: Constants.indent),
			simulateChangesButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
			simulateChangesButton.centerXAnchor.constraint(equalTo: centerXAnchor)
		])
	}

	@objc private func changeLayout(_ sender: UISegmentedControl) {
		layout = Layout(rawValue: sender.selectedSegmentIndex) ?? layout
	}

	@objc private func simulateChanges(_ sender: UIButton) {
		delegate?.contactView(self, didTapSimulateChangesButton: simulateChangesButton)
	}
}

// MARK: - UICollectionViewDelegate

extension ContactsView: UICollectionViewDelegate {

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		delegate?.contactView(self, didSelectItemAt: indexPath)
	}
}
