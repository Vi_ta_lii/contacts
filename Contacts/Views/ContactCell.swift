//
//  ContactCell.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit
import Combine

private struct Constants {
	static let avatarSize = 50
	static let onlineStatusImageSize: CGFloat = 10
}

final class ContactCell: UICollectionViewCell {

	// MARK: - Subviews

	let avatarImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.translatesAutoresizingMaskIntoConstraints = false
		return imageView
	}()

	private let onlineStatusImageView: UIImageView = {
		let imageView = UIImageView()
		imageView.translatesAutoresizingMaskIntoConstraints = false
		return imageView
	}()

	private let nameLabel: UILabel = {
		let label = UILabel()
		label.translatesAutoresizingMaskIntoConstraints = false

		return label
	}()

	// MARK: - Properties

	override var reuseIdentifier: String? {
		return toString(type(of: self))
	}

	var contactViewModel: ContactViewModel? {
		didSet {
			contactViewModel?.getAvatarImage(of: Constants.avatarSize)
			cancellable = contactViewModel?.$image.assignNoRetain(to: \.avatarImageView.image, on: self)
			onlineStatusImageView.image = contactViewModel?.onlineStatusImage
			nameLabel.text = contactViewModel?.name
		}
	}

	private var gridNameLabelWidthConstraint: NSLayoutConstraint?
	private var cancellable: AnyCancellable?

	// MARK: - Init

	override init(frame: CGRect) {
		super.init(frame: frame)
		commonInit()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	// MARK: - Overrides

	override func prepareForReuse() {
		super.prepareForReuse()
		contactViewModel?.cancelImageLoad()
		cancellable?.cancel()
	}

	override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
		super.apply(layoutAttributes)

		/*
		If you have a more complex production layout,
		better subclass UICollectionViewLayoutAttributes and UICollectionViewCompositionalLayout
		and add an attributes property you would check in apply(:rename) methods
		*/
		let isListLayout = layoutAttributes.frame.width > 50
		gridNameLabelWidthConstraint?.isActive = !isListLayout
	}

	// MARK: - Private methods

	private func commonInit() {
		addSubviews()
		addConstraints()
	}

	private func addSubviews() {
		[avatarImageView, onlineStatusImageView, nameLabel].forEach { contentView.addSubview($0) }
	}

	private func addConstraints() {
		gridNameLabelWidthConstraint = nameLabel.widthAnchor.constraint(equalToConstant: 0)
		NSLayoutConstraint.activate([
			avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
			avatarImageView.widthAnchor.constraint(equalToConstant: contentView.bounds.height),
			avatarImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

			onlineStatusImageView.centerXAnchor.constraint(equalTo: avatarImageView.trailingAnchor),
			onlineStatusImageView.centerYAnchor.constraint(equalTo: avatarImageView.bottomAnchor),
			onlineStatusImageView.widthAnchor.constraint(equalToConstant: Constants.onlineStatusImageSize),
			onlineStatusImageView.heightAnchor.constraint(equalTo: onlineStatusImageView.widthAnchor),

			nameLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 5),
			nameLabel.centerYAnchor.constraint(equalTo: avatarImageView.centerYAnchor)
		])
	}
}
