//
//  AppDelegate.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

}
