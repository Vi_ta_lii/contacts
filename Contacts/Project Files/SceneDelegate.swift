//
//  SceneDelegate.swift
//  Contacts
//
//  Created by Vitalii Kravets on 5/12/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?

	func scene(_ scene: UIScene,
			   willConnectTo session: UISceneSession,
			   options connectionOptions: UIScene.ConnectionOptions) {
		guard let windowScene = (scene as? UIWindowScene) else { return }

		let window = UIWindow(windowScene: windowScene)
		window.rootViewController = ContactsFlowController()
		self.window = window
		window.makeKeyAndVisible()
	}
}
