//
//  ContactsDataProviderTests.swift
//  ContactsUnitTests
//
//  Created by Vitalii Kravets on 5/27/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import XCTest
@testable import Contacts

class ContactsDataProviderTests: XCTestCase {

	var dataProvider: ContactsDataProvider?

    override func setUp() {
		super.setUp()
		dataProvider = ContactsDataProvider()
    }

    override func tearDown() {
		dataProvider = nil
		super.tearDown()
    }

    func testGeneratedContactsAmount() {
        let numberOfContactsToGenerate = [-2, 0, 1, 17, 50, 100, 872]
		let expectedNumberOfContactsGenerated = [0, 0, 1, 17, 50, 100, 100]

		let numberOfContactsGenerated = numberOfContactsToGenerate.map { dataProvider?.generateContacts(number: $0).count }

		XCTAssertEqual(expectedNumberOfContactsGenerated, numberOfContactsGenerated)
    }
}
