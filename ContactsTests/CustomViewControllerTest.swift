//
//  CustomViewControllerTest.swift
//  ContactsTests
//
//  Created by Vitalii Kravets on 6/4/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import XCTest
@testable import Contacts

class CustomViewControllerTest: XCTestCase {

	func testCutomViewIsOfGenericViewType() {
		XCTAssert(type(of: makeSUT(withViewType: UIView.self).customView) == UIView.self)
		XCTAssert(type(of: makeSUT(withViewType: ContactsView.self).customView) == ContactsView.self)
		XCTAssert(type(of: makeSUT(withViewType: ContactDetailView.self).customView) == ContactDetailView.self)
	}

	// MARK: - Helpers

	private func makeSUT<V: UIView>(withViewType type: V.Type) -> CustomViewController<V> {
		return CustomViewController<V>()
	}
}
