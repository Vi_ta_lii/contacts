//
//  URLProtocolStub.swift
//  ContactsUnitTests
//
//  Created by Vitalii Kravets on 5/27/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import Foundation

class URLProtocolStub: URLProtocol {

	static var testURLs = [URL?: Data]()

	override class func canInit(with request: URLRequest) -> Bool {
		return true
	}

	override class func canonicalRequest(for request: URLRequest) -> URLRequest {
		return request
	}

	override func startLoading() {
		guard let data = URLProtocolStub.testURLs[request.url] else {
			client?.urlProtocolDidFinishLoading(self)
			return
		}

		client?.urlProtocol(self, didLoad: data)
		client?.urlProtocolDidFinishLoading(self)
	}

	override func stopLoading() {

	}
}
