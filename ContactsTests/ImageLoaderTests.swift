//
//  ImageLoaderTests.swift
//  ContactsUnitTests
//
//  Created by Vitalii Kravets on 5/27/20.
//  Copyright © 2020 Vitalii Kravets. All rights reserved.
//

import XCTest
import Combine
@testable import Contacts

class ImageLoaderTests: XCTestCase {

	let testTimeout: TimeInterval = 3

	var sessionConfiguration: URLSessionConfiguration?
	var imageLoader: ImageLoader?
	var imageURL: URL?

    override func setUp() {
		super.setUp()
		sessionConfiguration = .ephemeral
		sessionConfiguration?.protocolClasses = [URLProtocolStub.self]

		guard let configuration = sessionConfiguration else {
			XCTFail("session configuration is nil")
			return
		}

		let session = URLSession(configuration: configuration)
		imageLoader = ImageLoader(session: session)
		imageURL = URL(string: "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200")
    }

    override func tearDown() {
		sessionConfiguration = nil
		imageLoader = nil
		imageURL = nil
		URLProtocolStub.testURLs = [:]
		super.tearDown()
    }

	func testCreate() throws {
		let url = try XCTUnwrap(imageURL)
		let bundle = Bundle(for: type(of: self))
		let data = try XCTUnwrap(UIImage(named: "test_gravatar.png", in: bundle, compatibleWith: nil)?.pngData())
		URLProtocolStub.testURLs = [url: data]

		let validTest = evalValidResponse(imageLoader: imageLoader, for: url)
        wait(for: validTest.expectations, timeout: testTimeout)
        validTest.cancellable?.cancel()
	}

	func evalValidResponse(imageLoader: ImageLoader?,
						   for url: URL) -> (expectations: [XCTestExpectation], cancellable: AnyCancellable?) {
		XCTAssertNotNil(imageLoader)

		let expectationFinished = expectation(description: "finished")
        let expectationReceive = expectation(description: "receiveValue")
        let expectationFailure = expectation(description: "failure")
		expectationFailure.isInverted = true

		let cancellable = imageLoader?.load(from: url).sink(receiveCompletion: { completion in
			switch completion {
			case .failure(let error):
				print("--TEST ERROR--")
                print(error.localizedDescription)
                print("------")
				expectationFailure.fulfill()
			case .finished:
				expectationFinished.fulfill()
			}
		}, receiveValue: { image in
			XCTAssertNotNil(image)
			expectationReceive.fulfill()
		})

		return (expectations: [expectationFinished, expectationReceive, expectationFailure], cancellable: cancellable)
	}
}
